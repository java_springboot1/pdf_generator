CURRENT_DIR=$(shell pwd)

APP=$(shell basename ${CURRENT_DIR})

APP_CMD_DIR=${CURRENT_DIR}/cmd

IMG_NAME=${APP}
REGISTRY=${REGISTRY}
TAG=${TAG}
TAG_LATEST=${TAG_LATEST}
GITLAB_URL=https://gitlab.hamkorbank.uz/
GITLAB_TOKEN=eefzzBNVwDb1-x8FR69H
PKG_LIST := $(shell go list ${CURRENT_DIR}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
YAML_FILES :=  $(shell find .  -iname *-ci.yml  | grep -v /vendor/)


.PHONY: all build build-image push-image swag-init dep build clean test coverage coverhtml lint-go lint-yaml

# Including
include .build_info

all: build

build: ## build
	CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${APP_CMD_DIR}/main.go

build-image: ##buid image
	docker build --rm -t ${REGISTRY}/${PROJECT_NAME}/${APP}/${IMG_NAME}:${TAG} .
	docker tag ${REGISTRY}/${PROJECT_NAME}/${APP}/${IMG_NAME}:${TAG} ${REGISTRY}/${PROJECT_NAME}/${APP}/${IMG_NAME}:${TAG_LATEST}

push-image: ## push image
	docker push ${REGISTRY}/${PROJECT_NAME}/${APP}/${IMG_NAME}:${TAG}
	docker push ${REGISTRY}/${PROJECT_NAME}/${APP}/${IMG_NAME}:${TAG_LATEST}

swag-init: ## init swagger
	swag init -g cmd/main.go -o api/docs

run: ## run application
	go run cmd/main.go

lint-go: ## Lint go the files
#	@golint -set_exit_status ${PKG_LIST}
	@golangci-lint run ./... || staticcheck -tests=false ./...
	
lint-yaml: ## Lint yaml files
	@chmod +x ./tools/gitlab-ci-validate  && ./tools/gitlab-ci-validate --host="${GITLAB_URL}" ${YAML_FILES}
	
test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@CC=clang go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: ## Get the dependencies
	@https_proxy=http://proxy.bank.local:8085 no_proxy=gitlab.hamkorbank.uz go get -v -d ./...

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
