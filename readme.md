## Сервис генерация документов

 Endpoint Swagger:
```
http://{host}:{port}/swagger-ui.html
```

 Endpoint для исползования Jasper:
```
/generate-pdf/use-jasper
```

 Endpoint для исползования Doc:
```
/generate-pdf/use-doc
```

 Пример запроса:
```json
{
    "bucket": "mybucket",
    "folder": "/",
    "templete_file": "Template.jrxml",
    "fields": {
        "bank_name": "Hamkorbank ATB"  
    }
}
```