FROM openjdk:12-alpine
COPY . ./
#RUN apk add  openjdk11

ENV http_proxy 'http://proxy.bank.local:8085'
ENV https_proxy 'http://proxy.bank.local:8085'

RUN apk update

RUN apk add --no-cache msttcorefonts-installer fontconfig
RUN update-ms-fonts

RUN apk add ttf-dejavu
RUN apk add maven

ENV MAVEN_OPTS '-Dhttp.proxyHost=proxy.bank.local -Dhttp.proxyPort=8085 -Dhttps.proxyHost=proxy.bank.local -Dhttps.proxyPort=8085'
RUN mvn clean install
RUN mvn clean package

RUN ls target/

RUN ls target/
#COPY target/generatedoc-0.0.1-SNAPSHOT.jar generatedoc.jar


ENTRYPOINT ["java","-jar","target/generatedoc-0.0.1-SNAPSHOT.jar"]


