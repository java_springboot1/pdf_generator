package com.hamkor.gendocpdf.controllers;



import com.google.gson.Gson;
import com.hamkor.gendocpdf.models.RequestDoc;
import com.hamkor.gendocpdf.models.RequestJasper;
import com.hamkor.gendocpdf.services.DocumentGeneratorAction;
import com.hamkor.gendocpdf.services.JasperAdapter;
import com.hamkor.gendocpdf.services.MinioAdapter;
import com.hamkor.gendocpdf.models.Responce;
import io.minio.errors.InvalidArgumentException;
import net.sf.jasperreports.engine.*;

import org.docx4j.Docx4J;
import org.docx4j.fonts.IdentityPlusMapper;
import org.docx4j.fonts.Mapper;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@RestController
@RequestMapping(path="/generate-pdf")
public class DocumentGenerationController {

    @Autowired
    MinioAdapter minioAdapter;

    @Autowired
    JasperAdapter jasperAdapter;



    @GetMapping(path = "/use-jasper")
    public void JasperGen(@RequestBody RequestJasper request, HttpServletResponse response) {

        try {
            response.setContentType("application/x-download");
            response.setHeader("Content-Disposition", String.format("attachment; filename=\"document.pdf\""));
            response.setStatus(HttpServletResponse.SC_OK);

            //set Stream for REsponce
            OutputStream out = response.getOutputStream();

            byte[] bytes = new byte[0];
            try {
                bytes = minioAdapter.getFile(request.getBucket(),request.getFolder(),request.getTemplete_file());
            } catch (Exception e) {
                e.printStackTrace();
            }
            InputStream stream = new ByteArrayInputStream(bytes);
            //Export file as Response or return error
            JasperExportManager.exportReportToPdfStream(jasperAdapter.exportPDF(stream,request.getFields()), out);

            } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @GetMapping(path = "/use-doc")
    public ResponseEntity<?> doc(@RequestBody RequestDoc data, HttpServletResponse response) {
        Gson gson = new Gson();
        JSONObject object = new JSONObject( gson.toJson(data.getFields()));
        System.out.println(object);

        HttpHeaders headers = new HttpHeaders();

        byte[] bytes = null;


        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            headers.add("Content-Disposition", "attachment;filename=document.pdf");
            //headers.add("Content-Type", "application/pdf");
            bytes= DocumentGeneratorAction.generateDocument(new ByteArrayInputStream(
                            minioAdapter.getFile(
                                    data.getBucket(),
                                    data.getFolder(),
                                    data.getTemplete_file())),
                                    object
                    ).toByteArray();

           ByteArrayInputStream inputStream= new ByteArrayInputStream(bytes);

           WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(inputStream);
           Mapper fontMapper = new IdentityPlusMapper();
           wordMLPackage.setFontMapper(fontMapper);
          //  fontMapper.put("Times-Bold", PhysicalFonts.get(some Chinese font installed in my OS));
           wordMLPackage.setFontMapper(fontMapper);
           MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart();
           Docx4J.toPDF(wordMLPackage,out);
           out.flush();
           out.close();



         return ResponseEntity.ok().headers(headers).body(out.toByteArray());
        } catch (Exception e) {
           e.printStackTrace();
        }

        headers.add("Content-Type", "application/json");
        return ResponseEntity.internalServerError().headers(headers).body(new Responce("-8787", "Fail to generate pdf using doc","FAIL"));

  }
    @GetMapping(path = "/ping")
    public String ForTest(@RequestBody String data, HttpServletResponse response) {
        return "pong";
    }
}