package com.hamkor.gendocpdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenDocPdfApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenDocPdfApplication.class, args);
    }

}
