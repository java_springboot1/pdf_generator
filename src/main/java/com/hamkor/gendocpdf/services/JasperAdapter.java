package com.hamkor.gendocpdf.services;


import com.google.gson.Gson;
import com.hamkor.gendocpdf.models.RequestJasper;
import io.minio.errors.*;
import net.sf.jasperreports.engine.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;


@Service
public class JasperAdapter {

    @Autowired
    MinioAdapter minioAdapter;

    public JasperPrint exportPDF(String data) {

        Gson gson = new Gson();
        RequestJasper objects = gson.fromJson(data, RequestJasper.class);



       byte[] bytes = new byte[0];
        try {
            bytes = minioAdapter.getFile(objects.getBucket(),objects.getFolder(),objects.getTemplete_file());
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        }
        InputStream stream = new ByteArrayInputStream(bytes);

        try {

            //set Templete file
            //JasperDesign jasperDesign = JRXmlLoader.load("C:\\Users\\HP\\JaspersoftWorkspace\\MyReports\\Coffee.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(stream);
            JRDataSource dataSource = new JREmptyDataSource();//JRBeanCollectionDataSource(Collections.EMPTY_LIST);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, objects.getFields(), dataSource);

            return jasperPrint;
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JasperPrint exportPDF(InputStream stream, Map<String,Object> parameters) {
        try {

            //set Templete file
            //JasperDesign jasperDesign = JRXmlLoader.load("C:\\Users\\HP\\JaspersoftWorkspace\\MyReports\\Coffee.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(stream);
            JRDataSource dataSource = new JREmptyDataSource();//JRBeanCollectionDataSource(Collections.EMPTY_LIST);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            return jasperPrint;
        } catch (JRException e) {
            e.printStackTrace();
        }
        return null;
    }
}
