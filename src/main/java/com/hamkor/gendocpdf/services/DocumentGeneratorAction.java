package com.hamkor.gendocpdf.services;
import io.minio.errors.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class DocumentGeneratorAction {

    @Autowired
    MinioAdapter minioAdapter;

    public InputStream getFile(String buckek, String folder, String templete_name){
        InputStream inputStream=null;
        try {
             inputStream = new ByteArrayInputStream(minioAdapter.getFile(buckek, folder,templete_name));
        } catch (InvalidArgumentException e) {
            e.printStackTrace();
        } catch (InvalidBucketNameException e) {
            e.printStackTrace();
        } catch (InsufficientDataException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoResponseException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InternalException e) {
            e.printStackTrace();
        }
        return inputStream;
    }

    public static ByteArrayOutputStream generateDocument(InputStream inputStream, JSONObject fields){
        /** used as reference: https://stackoverflow.com/a/49765239/5936443   [at 11/11/2020]
         This method is responsible for generating a document as a ByteArrayInputStream, using an exisiting word template at templatePath
         It replaces any keyTags in the document by the corresponding value in the JSONObject fields
         it assumes the keyTags come preceeded by the separator "{#" and proceeded by "#}", in the following form: {#keyTag#}
         */
        try {

            XWPFDocument doc = new XWPFDocument(OPCPackage.open(inputStream));

            // search in paragraphs
            for(XWPFParagraph p : doc.getParagraphs()){
                replaceFieldsParagraph(p, fields);
            }

            // search in tables
            for(XWPFTable t : doc.getTables()){
                replaceFieldsTable(t, fields);
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            doc.write(out);
//            ByteArrayInputStream inputStream = new ByteArrayInputStream(out.toByteArray());
            return out;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        return null;
    }




    public static void replaceFieldsParagraph(XWPFParagraph paragraph, JSONObject fields){
        /** this method is responsible for replacing any ocurrences in the paragraph of any of the keyTags
         * present in the JSONObject fields by the corresponding value */
        String text = paragraph.getText(); //all the text from each run concatenated
        String findStr;
        if( !text.contains("{#")) //paragraph doesn't have keys to replace
            return;

        // for each field to replace, search it in the curr paragraph
        for( String key : fields.keySet()){
            findStr = "{#" + key + "#}";
            // if paragraph doesn't have current key, we skip to next key
            if( text.contains(findStr)) {
                mergeRunsWithSplittedKeyTags(paragraph);
                for (XWPFRun run : paragraph.getRuns()) {
                    Object obj = fields.get(key);
                    if (obj instanceof JSONArray)
                    {
                        // it's an array
                        JSONArray array = (JSONArray) obj;
                        List<Object> list = array.toList();

                        StringBuilder sb = new StringBuilder();
                        int i = 0;
                        for (Object arr:list) {
                            i++;
                            sb.append(i+") "+String.valueOf(arr)+"\n");
                        }

                        String runText = run.getText(0);
                        if( runText!= null && runText.contains(findStr)){
                            runText = runText.replace(findStr, sb.toString());
                            run.setText(runText, 0);
                        }
                    }
                    else {
                        // check if current run has current key
                        checkAndReplaceFieldRun(run, findStr, String.valueOf(obj));
                    }
                }
            }
        }
    }

    public static void replaceFieldsTable(XWPFTable table, JSONObject fields){
        /** this method is responsible for replacing any ocurrences in the table of any of the keyTags
         * present in the JSONObject fields by the corresponding value */

        if( table.getNumberOfRows() > 0){
            for(XWPFTableRow row : table.getRows()){                                        // iterate over rows
                for( XWPFTableCell cell : row.getTableCells()){                             // iterate over columns
                    if( cell.getParagraphs() != null && cell.getParagraphs().size()>0){
                        for(XWPFParagraph paragraph : cell.getParagraphs()){                // get cell paragraphs
                            replaceFieldsParagraph(paragraph, fields);                      // replacing existing keyTags in paragraph
                        }
                    }
                }
            }
        }
    }

    public static void checkAndReplaceFieldRun(XWPFRun run, String findStr, String value){
        String runText = run.getText(0);
        if( runText!= null && runText.contains(findStr)){
            runText = runText.replace(findStr, value);
            run.setText(runText, 0);
        }
    }

    public static void mergeRunsWithSplittedKeyTags(XWPFParagraph paragraph){
        /**
         A run is a part of the paragraph that has the same formatting.
         Word separates the text in paragraphs by different runs in a almost 'random' way,
         sometimes the tag we are looking for is splitted across multiple runs.
         This method merges the runs that have a keyTag or part of one,
         so that the keyTag starting with "{#" and ending with "#}" is in the same run
         */
        String runText;
        XWPFRun run, nextRun;

        List<XWPFRun> runs = paragraph.getRuns();

        for( int i=0 ; i<runs.size(); i++){
            run = runs.get(i);
            runText = run.getText(0);
            if( runText != null &&
                    (runText.contains("{#") ||  // current run has the complete separator "{#"
                            (runText.contains("{") && (runs.get(i + 1).getText(0)!=null && runs.get(i + 1).getText(0).substring(0, 1).equals("#"))))){   //current run has the first char, next run has the second char

                while( !openTagMatchesCloseTag(runText) ){
                    nextRun = runs.get(i + 1);
                    runText = runText + nextRun.getText(0);
                    paragraph.removeRun(i + 1);
                }
                run.setText(runText, 0); // if we don't set with arg pos=0 it doesn't replace the contents, it adds to them and repeats chars
            }
        }
    }

    public static boolean openTagMatchesCloseTag(String runText){
        /** This method validates if we have a complete run.
         * Either by having no keyTags present, or by having a complete keyTag.
         * If we have parts of a keyTag, but not the complete one, returns false.*/
        int incompleteOpenTagCount = runText.split("\\{", -1).length - 1;   // "{"
        int completeOpenTagCount = runText.split("\\{#", -1).length - 1;    // "{#"
        int completeCloseTagCount = runText.split("#}", -1).length - 1;     // "#}"

        if(completeOpenTagCount>0){  // we already have open and close tags, compare the counts
            return completeOpenTagCount == completeCloseTagCount;
        } else {
            if( incompleteOpenTagCount>0 ){   // we only have a "{" not the whole "{#"
                return false;
            }
        }

        //doesn't have neither "{" nor "{#", so there's no need to close tags
        return true;
    }

}