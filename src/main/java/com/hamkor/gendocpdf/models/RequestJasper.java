package com.hamkor.gendocpdf.models;

import java.util.HashMap;

public class RequestJasper {



        private  String bucket;
        private  String folder;
        private  String templete_file;
        private HashMap<String,Object> fields;

    public RequestJasper(String buckek, String folder, String templete_file, HashMap<String,Object> fields) {
        this.bucket = buckek;
        this.folder = folder;
        this.templete_file = templete_file;
        this.fields = fields;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getTemplete_file() {
        return templete_file;
    }

    public void setTemplete_file(String templete_file) {
        this.templete_file = templete_file;
    }

    public HashMap<String,Object> getFields() {
        return fields;
    }

    public void setFields(HashMap<String,Object> fields) {
        this.fields = fields;
    }
}
