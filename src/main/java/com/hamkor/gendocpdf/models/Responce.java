package com.hamkor.gendocpdf.models;

public class Responce {
    String error_code;
    String error_note;
    String status;

    public Responce(String error_code, String error_note, String status) {
        this.error_code = error_code;
        this.error_note = error_note;
        this.status = status;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_note() {
        return error_note;
    }

    public void setError_note(String error_note) {
        this.error_note = error_note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
